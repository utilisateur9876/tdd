package fr.ynov.tdd.fizzbuzz;

public class FizzBuzz {

    public static String divisible(Integer number) {

        String result = "";

        if(number % 3 == 0) {
            result += "Fizz";
        }
        if(number % 5 == 0) {
            result += "Buzz";
        }
        if (result == "") {
            result = String.valueOf(number);
        }

        return result;
    }
}
