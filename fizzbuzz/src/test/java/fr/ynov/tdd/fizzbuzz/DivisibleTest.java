package fr.ynov.tdd.fizzbuzz;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Unit test for simple App.
 */
public class DivisibleTest {


    /**
     * Create list result of program
     * @return List<string> listresult
     */
    private List<String> printResultOfListNumber(Integer number) {
        List<String> listResult = new ArrayList<>();

        for (int i = 1; i < number +1; i++) {
            listResult.add(FizzBuzz.divisible(i));
        }

        return listResult;
    }

    /**
     * Should return not divisible by X
     */
    @Test
    public void shouldReturnNotDivisableByX() {
        assertEquals("1", FizzBuzz.divisible(1));
    }

    /**
     * Should return divisible by 3
     */
    @Test
    public void shouldReturnDivisableBy3() {
        assertEquals("Fizz", FizzBuzz.divisible(3));
    }

    /**
     * Should return divisible by 3
     */
    @Test
    public void shouldReturnDivisableBy5() {
        assertEquals("Buzz", FizzBuzz.divisible(5));
    }

    /**
     * Should return divisible by 3
     */
    @Test
    public void shouldReturnDivisableBy3and5() {
        assertEquals("FizzBuzz", FizzBuzz.divisible(15));
    }

    /**
     * Should return result 0 - 20
     */
    @Test
    public void shouldReturnGoodResultStart0Of20() {

        // Résultat attendu par l'exercice
        List<String> result = Arrays.asList("1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz", "16", "17", "Fizz", "19", "Buzz");
        assertEquals(result, printResultOfListNumber(20));
    }

}