package fr.ynov.tdd.leapyears;

public class LeapYears {

    public static String calculate(Integer year) {

        String result = "";

        if (year % 400 == 0  || year % 4 == 0 && year % 100 != 0) {
            result = year + " is leap years";
        } else {
            result = year + " is not leap years because not divisible by 4 or 400";
        }

        if (year % 100 == 0 && year % 400 != 0) {
            result = year + " is not leap years because divisible by 100 and not by 400";
        }

        return result;
    }
}
