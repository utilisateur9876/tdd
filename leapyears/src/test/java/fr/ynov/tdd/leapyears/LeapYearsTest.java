package fr.ynov.tdd.leapyears;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LeapYearsTest {

    /**
     * Should return x is leap years if divisible by 400
     */
    @Test
    public void shouldReturnXIsForLeapYearsIfDivisibleBy100() {
        Integer year = 2000;
        assertEquals(year + " is leap years", LeapYears.calculate(year));
    }

    /**
     * Should return x is not leap years if divisible by 100 and not by 400
     */
    @Test
    public void shouldReturnXIsNotLeapYearsIfDivisibleBy100AndNotBy400() {
        Integer year = 1700;
        assertEquals(year + " is not leap years because divisible by 100 and not by 400", LeapYears.calculate(year));
    }

    /**
     * Should return x is leap years if divisible by 4 and not by 100
     */
    @Test
    public void shouldReturnXIsLeapYearsIfDivisibleBy4AndNotBy100() {
        Integer year = 2008;
        assertEquals(year + " is leap years", LeapYears.calculate(year));
    }

    /**
     * Should return x is not leap years if divisible by 4
     */
    @Test
    public void shouldReturnXIsNotLeapYearsIfDivisibleBy4() {
        Integer year = 2017;
        assertEquals(year + " is not leap years because not divisible by 4 or 400", LeapYears.calculate(year));
    }

}

